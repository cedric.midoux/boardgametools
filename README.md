
<!-- README.md is generated from README.Rmd. Please edit that file -->

# boardgametools

<!-- badges: start -->
<!-- badges: end -->

The goal of boardgametools is to …

## Installation

You can install the development version of boardgametools like so:

``` r
# FILL THIS IN! HOW CAN PEOPLE INSTALL YOUR DEV PACKAGE?
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(boardgametools)
## boardgametools::run_app()
```

## Deploy

### Avec docker :

-   ajout du `.gitlab-ci.yml`
-   construction de l’image par les runners de la forge
-   pull :
    `docker pull registry.forgemia.inra.fr/cedric.midoux/boardgametools`
-   run :
    `docker run -p 3838:3838 registry.forgemia.inra.fr/cedric.midoux/boardgametools`

### Installation depuis GitLab

-   `remotes::install_gitlab(repo = "cedric.midoux/boardgametools", host = "forgemia.inra.fr")`

## Code of Conduct

Please note that the boardgametools project is released with a
[Contributor Code of
Conduct](https://contributor-covenant.org/version/2/0/CODE_OF_CONDUCT.html).
By contributing to this project, you agree to abide by its terms.
